import java.util.*;

class NeutralRelay {

  private Map<Boolean, List<int[]>> contacts = new HashMap<Boolean, List<int[]>>();
  private Boolean innerState = Boolean.FALSE;

  NeutralRelay(int[][] contactGroups){
    List<int[]> pairsOn = new ArrayList<int[]>();
    List<int[]> pairsOff = new ArrayList<int[]>();
    for (int[] group : contactGroups) {
      pairsOff.add(new int[] { group[0], group[1] });
      pairsOn.add(new int[] { group[1], group[2] });
    }
    contacts.put(Boolean.FALSE, pairsOff);
    contacts.put(Boolean.TRUE, pairsOn);
  }

  public String state(){ return innerState ? "ON" : "OFF"; }

  public void powerOn(){ innerState = Boolean.TRUE; }

  public void powerOff(){ innerState = Boolean.FALSE; }

  public List<int[]> connectedContacts(){ return contacts.get(innerState); }

  public int contactConnectedTo(int contact){
    for(int[] pair : connectedContacts()){ 
      if(pair[0] == contact){ return pair[1]; }
      if(pair[1] == contact){ return pair[0]; }
    }
    return 0;
  }

  public boolean areConnected(int contact1, int contact2){
    return contactConnectedTo(contact1) == contact2;
  }
  
}

/////////////////// Example of using (relay HH54P)

class Test {
  // There is no standard method to print 2-dimensional array...
  private static void printConnectedContacts(NeutralRelay relay){
    List<String> pairStrings = new ArrayList<String>();
    for(int[] pair : relay.connectedContacts()){ 
      pairStrings.add( Arrays.toString(pair) ); 
    }
    System.out.println("Connected contacts: " + Arrays.toString(pairStrings.toArray())); 
  }
  
  private static void testContact(NeutralRelay relay, int contact){
    System.out.println(contact + " is connected to " + relay.contactConnectedTo(contact));
  }

  private static void testContactPair(NeutralRelay relay, int contact1, int contact2){
    System.out.println(contact1 + " and " + contact2 + " are connected: " +  relay.areConnected(contact1, contact2));
  }

  private static void test(NeutralRelay relay){
    System.out.println("State: " + relay.state());
    printConnectedContacts(relay);
    testContact(relay, 1);
    testContact(relay, 9);
    testContact(relay, 5);
    testContactPair(relay, 1,9);
    testContactPair(relay, 9,1);
    testContactPair(relay, 9,5);
  }

  public static void main (String[] args){
    int[][] contactGroups =  { {1, 9, 5}, {2, 10, 6}, {3, 11, 7}, {4, 12, 8} };
    NeutralRelay relay = new NeutralRelay(contactGroups);
    test(relay);    
    relay.powerOn();
    test(relay);
  }
}
