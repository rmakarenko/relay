NeutralRelay = {
  
  init: function(contactGroups){
    this._contacts = {
      false: contactGroups.map(function(group){ return group.slice(0,2); }),
      true: contactGroups.map(function(group){ return group.slice(1,3); })
    }
    this.powerOff();
    return this;
  },
  
  state: function(){ return this._state ? "ON" : "OFF"; },
  
  powerOn: function(){ this._state = true; },
  
  powerOff: function(){ this._state = false; },
  
  connectedContacts: function(){ return this._contacts[this._state]; },
  
  contactConnectedTo: function(contact){
    var pair = this.connectedContacts().filter(function(pair){ 
      return pair.some(function(c){ return c == contact; });
    })[0];
    return pair ? pair.filter(function(c){ return c != contact; })[0] : null;
  },
  
  connected: function(contact1, contact2){
    return this.contactConnectedTo(contact1) == contact2;
  }
  
};

/////////////////// Example of using (relay HH54P)

var contactGroups = [
  [1, 9, 5],
  [2, 10, 6], 
  [3, 11, 7], 
  [4, 12, 8]
]
var relay = Object.create(NeutralRelay).init(contactGroups);

var testContact = function(contact){
  document.write(contact + ' is connected to ' + relay.contactConnectedTo(contact) + '<br/>');
};

var testContactPair = function(contact1, contact2){
  document.write(contact1 + ' and ' + contact2 + ' are connected: ' + relay.connected(contact1, contact2) + '<br/>');
};

var test = function(){
  document.write('State: ' + relay.state()  + '<br/>');
  document.write('connectedContacts: ' + JSON.stringify(relay.connectedContacts())  + '<br/>');
  testContact(1);
  testContact(9);
  testContact(5);
  testContactPair(1,9);
  testContactPair(9,1);
  testContactPair(9,5);
};

test();
relay.powerOn();
test();
